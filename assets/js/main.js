$(window).load(function(){

    $('.bxslider').bxSlider({
        mode: 'vertical',
        pager: false,
        speed: 600,
        controls: false,
        auto: true,
        pause: 2000,
        onSliderLoad: function(){
            $('#ffs-header').addClass('visible');
        }
    });

   var tableHeight = $('.ffs-quad-grid a').outerHeight();
    if ( $(window).width() > 768 ) {
        $('.ffs-overlay').css('height', tableHeight);
    } else {
        $('.ffs-overlay').css('height', 'auto');
    }

});

$(window).resize(function(){
    var tableHeight = $('.ffs-quad-grid a').outerHeight();
    if ( $(window).width() > 768 ) {
        $('.ffs-overlay').css('height', tableHeight);
    } else {
        $('.ffs-overlay').css('height', 'auto');
    }

}).resize();

imagesLoaded( '#ffs-container', function(){
    $('.ffs-logo-drop').addClass('visible slideInDown');
});