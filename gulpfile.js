var gulp = require('gulp'),
gutil = require('gulp-util'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
watch = require('gulp-watch'),
bower = require('gulp-bower'),
usemin = require('gulp-usemin');

gulp.task('default', function(){
	gulp.src('index.html')
		.pipe(usemin({
			//assetsDir: 'assets',
			js: [uglify(), 'concat']
		}))
		.pipe(gulp.dest('public'));
});